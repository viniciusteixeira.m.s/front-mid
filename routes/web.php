<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Example Routes
// Route::view('/', 'landing');
// Route::match(['get', 'post'], '/dashboard', function(){
//     return view('dashboard');
// });
// Route::view('/pages/slick', 'pages.slick');
// Route::view('/pages/datatables', 'pages.datatables');
// Route::view('/pages/blank', 'pages.blank');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/welcome', function () {
    return view('welcome');
});

// Views
Route::get('/', 'PagesController@index')->name('index');
Route::get('/sobre', 'PagesController@sobre')->name('sobre');
Route::get('/faq', 'PagesController@faq')->name('faq');
Route::get('/termos', 'PagesController@termos')->name('termos');
Route::get('/politica', 'PagesController@politica')->name('politica');
Route::get('/pagamento-aprovado', 'PagesController@pagamentoAprovado')->name('pagamentoAprovado');
Route::get('/cursos-single', 'PagesController@cursosSingle')->name('cursosSingle');
Route::get('/cursos-mid', 'PagesController@cursosMid')->name('cursosMid');
Route::get('/cursos-login', 'PagesController@cursosLogin')->name('cursosLogin');
Route::get('/cursos-concluido', 'PagesController@cursosConcluido')->name('cursosConcluido');
Route::get('/cursos-cadastro', 'PagesController@cursosCadastro')->name('cursosCadastro');
Route::get('/cursos-cadastro-ok', 'PagesController@cursosCadastroOk')->name('cursosCadastroOk');
Route::get('/cursos-avaliacao-mid', 'PagesController@cursosAvaliacao')->name('cursosAvaliacaoMid');
Route::get('/cursos-aula', 'PagesController@cursosAula')->name('cursosAula');
Route::get('/contato', 'PagesController@contato')->name('contato');
Route::get('/login-adm', 'PagesController@loginAdm')->name('loginAdm');

Auth::routes();

//Admin

Route::prefix('admin')->group(function(){
    Route::view('/', 'admin.login');
});


// Route::middleware('auth')->group(function(){
//     // Route::get('/admin', 'HomeController@index')->name('admin'); 
//     Route::prefix('admin')->group(function(){
//         Route::get('/', function(){
//             return view('admin.dashboard');
//         });

//         Route::prefix('user')->group(function(){
//             Route::get('/', function(){
//                 return view('admin.users.list');
//             });
//             Route::get('/create', function(){
//                 return view('admin.users.form');
//             });
//         });
//         Route::prefix('groups')->group(function(){
//             Route::get('/', function(){
//                 return view('admin.groups.list');
//             });
//             Route::get('/create', function(){
//                 return view('admin.groups.form');
//             });
//         });
//         Route::prefix('objects')->group(function(){
//             Route::get('/', function(){
//                 return view('admin.objects.list');
//             });
//             Route::get('/create', function(){
//                 return view('admin.objects.form');
//             });
//         });
//         Route::prefix('themes')->group(function(){
//             Route::get('/', function(){
//                 return view('admin.themes.list');
//             });
//             Route::get('/create', function(){
//                 return view('admin.themes.form');
//             });
//         });
//         Route::prefix('reports')->group(function(){
//             Route::get('/', function(){
//                 return view('admin.reports.dashboard');
//             });
//         });
//         Route::prefix('classes')->group(function(){
//             Route::get('/', function(){
//                 return view('admin.classes.list');
//             });
//             Route::get('/create', function(){
//                 return view('admin.classes.form');
//             });
//         });
//     });
    
   
// });


Route::middleware('auth')->group(function(){
    Route::get('/home', function(){
        return view('admin.dashboard');
    });
    // Route::get('/admin', 'HomeController@index')->name('admin'); 
    Route::prefix('admin')->group(function(){
        Route::get('/dashboard', function(){
            return view('admin.dashboard');
        });
        Route::get('/', function(){
            return view('admin.dashboard');
        });
       

        Route::prefix('user')->group(function(){
            Route::get('/', function(){
                return view('admin.users.list');
            });
            Route::get('/create', function(){
                return view('admin.users.form');
            });
            Route::post('/store', 'UserController@store')->name('user.store');
        });
        Route::prefix('groups')->group(function(){
            Route::get('/', function(){
                return view('admin.groups.list');
            });
            Route::get('/create', function(){
                return view('admin.groups.form');
            });
        });
        Route::prefix('trails')->group(function(){
            Route::get('/', function(){
                return view('admin.trails.list');
            });
            Route::get('/create', function(){
                return view('admin.trails.form');
            });
        });
        Route::prefix('themes')->group(function(){
            Route::get('/', function(){
                return view('admin.themes.list');
            });
            Route::get('/create', function(){
                return view('admin.themes.form');
            });
        });
        Route::prefix('objects')->group(function(){
            Route::get('/', function(){
                return view('admin.objects.list');
            });
            Route::get('/create', function(){
                return view('admin.objects.form');
            });
        });
        Route::prefix('reports')->group(function(){
            Route::get('/', function(){
                return view('admin.reports.dashboard');
            });
        });
        Route::prefix('classes')->group(function(){
            Route::get('/', function(){
                return view('admin.classes.list');
            });
            Route::get('/create', function(){
                return view('admin.classes.form');
            });
        });
        Route::prefix('payments')->group(function(){
            Route::get('/', function(){
                return view('admin.payments.list');
            });
        });
        Route::prefix('certificates')->group(function(){
            Route::get('/', function(){
                return view('admin.certificates.list');
            });
        });

        Route::prefix('comments')->group(function(){
            Route::get('/create', function(){
                return view('admin.comments.form');
            });
            Route::get('/', function(){
                return view('admin.comments.list');
            });
        });
        Route::prefix('gamification')->group(function(){
            Route::get('/create', function(){
                return view('admin.gamification.form');
            });
            Route::get('/', function(){
                return view('admin.gamification.list');
            });
        });
        Route::prefix('feeds')->group(function(){
            Route::get('/create', function(){
                return view('admin.feeds.form');
            });
            Route::get('/', function(){
                return view('admin.feeds.list');
            });
            Route::get('/comments', function(){
                return view('admin.feeds.comments');
            });
        });
    });
    
   
});


// Example Routes
// Route::view('/', 'landing');
Route::match(['get', 'post'], '/dashboard', function(){
    return view('admin.dashboard');
});
Route::view('/pages/slick', 'admin.pages.slick');
Route::view('/pages/datatables', 'admin.pages.datatables');
Route::view('/pages/blank', 'admin.pages.blank');


