@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<!-- <script>
// Window.Echo.private('activity.' + this.user.id)
//     .listen('ActivityLogged', (e) => {
//         //push to feed variable
//     });
$(function(){
    Echo.channel('messages')
    .listen('.newMessage', (message) => {
       // this.messages.push(message);
       alert('cade')
    });    

})


</script> -->