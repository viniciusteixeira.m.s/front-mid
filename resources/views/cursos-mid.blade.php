    @extends('layouts.site')

@section('header')
@endsection

@section('content') 

<div class="container-fluid pr-0 pl-0">
    <div class="row d-flex justify-content-center mb-5 pb-5">
        <h1 class="bordersection text-center pt-5 mb-5 pb-5 font h1cursosmid d-none d-lg-block top1200CursosMid mbdiv"> APRENDA SOBRE O MERCADO</h1>
        <h1 class="bordersection text-center pt-5 mb-5 pb-5 font h1cursosmid d-md-block d-lg-none"> APRENDA SOBRE O<br/> MERCADO</h1>
            <img src="{{ asset('images/banner-2.jpg') }}" class="d-none d-xl-block bgbanner1"/>
            <img src="{{ asset('images/cursos_mid.jpg') }}" class="d-block d-md-none bgbanner1"/>
            <img src="{{ asset('images/CursosMid992.jpg') }}" class="d-none d-lg-block d-xl-none bgbanner1"/>
            <img src="{{ asset('images/cursosMid.jpg') }}" class="d-none d-md-block d-lg-none bgbanner1"/>

        <div class="container">
            <div class="row paddingmob">
                <h1 class="text-white text-left font fontmobile d-none d-lg-block font-weight-bold PCursosMid5120"> TÍTULO DO CURSO | <span class="textcolor font-weight-light"> Professor do Curso</span></h1>
                <h1 class="text-white text-left font fontmobile d-md-block d-lg-none mb-3 font-weight-bold textMid768"> TÍTULO DO CURSO  <br/><span class="textcolor font-weight-light"> Professor do Curso</span></h1>

                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pr-0 pl-0">
    <div class="row d-flex justify-content-center mb-5 pb-5">
            <img src="{{ asset('images/banner-cursos-2.jpg') }}" class="d-none d-xl-block bgbanner1"/>
            <img src="{{ asset('images/cursos_mid2.jpg') }}" class="d-block d-md-none bgbanner1"/>
            <img src="{{ asset('images/CursosMid992-2.jpg') }}" class="d-none d-lg-block d-xl-none bgbanner1"/>
            <img src="{{ asset('images/CursosMid2.jpg') }}" class="d-none d-md-block d-lg-none bgbanner1"/>

        <div class="container">
            <div class="row paddingmob">
                <h1 class="text-white text-left font fontmobile d-none d-lg-block font-weight-bold PCursosMid5120"> TÍTULO DO CURSO | <span class="textcolor font-weight-light"> Professor do Curso</span></h1>
                <h1 class="text-white text-left font fontmobile d-md-block d-lg-none mb-3 font-weight-bold textMid768"> TÍTULO DO CURSO  <br/><span class="textcolor font-weight-light"> Professor do Curso</span></h1>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid pr-0 pl-0">
    <div class="row d-flex justify-content-center mb-5 pb-5">
            <img src="{{ asset('images/banner-cursos-3.jpg') }}" class="d-none d-xl-block bgbanner1"/>
            <img src="{{ asset('images/cursos_mid23jpg.jpg') }}" class="d-block d-md-none bgbanner1"/>
            <img src="{{ asset('images/CursosMid3.jpg') }}" class="d-none d-md-block d-lg-none bgbanner1"/>
            <img src="{{ asset('images/CursosMid992-3.jpg') }}" class="d-none d-lg-block d-xl-none bgbanner1"/>

        <div class="container">
            <div class="row paddingmob">
                <h1 class="text-white text-left font fontmobile d-none d-lg-block font-weight-bold PCursosMid5120"> TÍTULO DO CURSO | <span class="textcolor font-weight-light"> Professor do Curso</span></h1>
                <h1 class="text-white text-left font fontmobile d-md-block d-lg-none mb-3 font-weight-bold textMid768"> TÍTULO DO CURSO  <br/><span class="textcolor font-weight-light"> Professor do Curso</span></h1>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
                <p class="textcolorP">Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. 
                Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')
@endsection