@extends('layouts.adm')

@section('header')
@endsection

@section('content') 
<div class="container-fluid pr-0 pl-0 pb-sm-0">
    <img src="{{ asset('images/login.jpg') }}" class="bgbanner1"/>

    <div class="row justify-content-center formcenter">
        <div class="row justify-content-center">
            <img src="{{ asset('images/LOGOadmin.png') }}" style="width: 32%; margin-bottom: 100px" class="bgbanner1 d-none d-xl-block"/>
            <img src="{{ asset('images/LOGOadmin.png') }}" style="width: 55%; margin-bottom: 100px" class="bgbanner1 d-block d-xl-none"/>
        </div>
        <div class="form-group row justify-content-center mb-4">
            <div class="col-md-12 col-sm-12 col-lg-10 font">
                <input id="name" type="name" class="font formAdm @error('E-mail') is-invalid @enderror" name="name" placeholder="Login"  autofocus style="border-radius: 0">

                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row justify-content-center pt-4">
            <div class="col-md-12 col-sm-12 col-lg-10 font">
                <input id="name" type="name" class="font formAdm @error('E-mail') is-invalid @enderror" name="name" placeholder="Senha"  autofocus style="border-radius: 0">

                    <span class="invalid-feedback" role="alert">
                        <strong></strong>
                    </span>
            </div>
        </div>
        <div class="col-md-12 col-lg-11 col-sm-12 text-right mr-0 pr-0 mt-5">
            <button type="submit font" class="btn btn-outline-warning btnadmin">
                {{ __('ENTRAR') }}
            </button>
        </div>
    </div>
</div>
@endsection 

@section('footer')

@endsection

@section('script')

@endsection

<style>
    ::-webkit-input-placeholder {
  color: #484848 !important;
  font-weight: 100;
  font-family: "HelveticaNeue";
  font-size: 20px;
}
</style>