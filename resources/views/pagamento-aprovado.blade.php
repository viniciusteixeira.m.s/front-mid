@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid pr-0 pl-0">
    <img src="{{ asset('images/banner-pagamento.jpg') }}" class="bgbanner1 d-none d-xl-block"/>
    <img src="{{ asset('images/pagamento992.jpg') }}" class="bgbanner1 d-none d-lg-block d-xl-none"/>
    <img src="{{ asset('images/pagamento.jpg') }}" class="bgbanner1 d-block d-md-none"/>
    <img src="{{ asset('images/pagamento768.jpg') }}" class="bgbanner1 d-none d-md-block d-lg-none"/>

</div>
<div class="container-fluid">
    <div class="row sobremb">
        <div class="col-lg-9 col-sm-12 col-xl-7 offset-lg-1 aprovadoMobile">
            <h1 class="text-left text-white font-weight-bold font text-uppercase contatosobre d-none d-md-block h1pagto5120">Aguardando a confirmação de pagamento</h1>
            <h1 class="text-left text-white font-weight-bold font text-uppercase contatosobre d-block d-md-none">Aguardando a <br/>confirmação <br/>de pagamento</h1>

            <p class="font pagtp p992 pagtP5120">Estamos aguardando a confirmação do seu pagamento. Assim <br/>que ele for confirmado, te enviaremos um e-mail confirmando!</p>

        </div>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')
@endsection