@extends('layouts.site')

@section('content')
<div class="container-fluid mt-5 pt-5 offset-1">
    <div class="row justify-content-left">
        <div class="col-md-7">
            <div class="card bgLogin">
                <h1 class="card-header text-white font-weight-bold font h1Login5120">{{ __('FAÇA SEU LOGIN') }}</h1>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row" style="display: flow-root ">
                            <label for="email" class="col-md-4 col-form-label text-md-left text-white font labelLogin5120">{{ __('Email') }}</label>

                            <div class="col-md-8 font">
                                <input id="email" type="email" class="font form-control @error('E-mail') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus style="border-radius: 0">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" style="display: flow-root ">
                            <label for="password" class="col-md-4 col-form-label text-md-left text-white font labelLogin5120">{{ __('Senha') }}</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="font form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" style="border-radius: 0">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
{{-- 
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group row mb-0 text-left mt-5">
                            <div class="col-md-4">
                                @if (Route::has('password.request'))
                                    <a class="text-left font" id="link" href="{{ route('password.request') }}">
                                        {{ __('Esqueceu sua senha?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-4 text-right">
                                <button type="submit font" class="btn btn-outline-warning btnenviar">
                                    {{ __('ENTRAR') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<style>
    .headerposition {
        position: relative !important;
    }
    .footerbg {
        position: absolute !important;
        bottom: 0;
    }
    </style>

