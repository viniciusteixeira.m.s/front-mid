@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container mt-5 pt-5">
    <div class="row ml-1 mbdiv">
        <h1 class="text-white text-left mt-5 font d-none d-xl-block font titlePolitica font-weight-bold"> POLÍTICA DE PRIVACIDADE</h1>
        <h1 class="text-white text-left mt-5 font d-block d-xl-none font font-weight-bold politicaH1"> POLÍTICA DE <br/>PRIVACIDADE</h1>

    </div>
    <div class="row">
            <div class="col-lg-12 col-12 col-xl-7 mt-4 mb-5 pb-5">
            <p class="textoPolitica">Mussum Ipsum, cacilds idis litro abertis. Suco de cevadiss deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
            </p>
            <p class="textoPolitica"> Detraxit consequat et quo num tendi nada. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Paisis, filhis, espiritis santis.
            </p>   
            <p class="textoPolitica">  Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis. Delegadis gente finis, bibendum egestas augue arcu ut est. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
            </p>
            <p class="textoPolitica">Mussum Ipsum, cacilds vidis litro abertis. <a href="#" id="link"> Suco de cevadiss</a> deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
            </p>
            <p class="textoPolitica">Detraxit consequat et quo num tendi nada. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Paisis, filhis, espiritis santis.
            </p>   
            <p class="textoPolitica"> Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis. Delegadis gente finis, bibendum egestas augue arcu ut est. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
            </p>
            <p class="textoPolitica">Mussum Ipsum, cacilds vidis litro abertis. Suco de cevadiss deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
            </p>
            <p class="textoPolitica">Detraxit consequat et quo num tendi nada. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Paisis, filhis, espiritis santis.
            </p>   
            <p class="textoPolitica"> Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis. Delegadis gente finis, bibendum egestas augue arcu ut est. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
            </p>
            </div>
    </div>

</div>@endsection

@section('footer')
@endsection

@section('script')
@endsection

<style>
    .headerposition{
        position: relative !important;
    }
    </style>

