@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container mt-5 pt-5">
    <div class="row mt-5 okmb">
        <div class="col-lg-12 col-10 mt-5 pt-5">
            <h1 class="text-white text-left mt-5 font d-none d-xl-block ajustetitulo tituloOK1200"> SEU CADASTRO FOI CONCLUÍDO COM SUCESSO!</h1>
            <h1 class="text-white text-left mt-5 font d-block d-xl-none ajustetitulo"> SEU CADASTRO <br/>FOI CONCLUÍDO <br/>COM SUCESSO!</h1>
        </div>
        <div class="col-lg-9 col-12 d-none d-md-block pr-0 mt-5">
            <div class="text-right mt-5 divCursosOK1200">
                <button type="button" class="btn btn-outline-warning btnenviar font fontOk cadastroOk5120">IR PARA A PÁGINA DE CURSOS</button>
            </div>
        </div>
        <div class="col-lg-10 col-12 d-block d-md-none">
            <div class="text-right mt-5">
                <button type="button" class="btn btn-outline-warning btnenviar font fontOk">IR PARA A PÁGINA DE CURSOS</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')
@endsection

<style>
    @media screen and (min-width: 576px){
        .footerbg {
        position: absolute !important;
        bottom: 0;
    }
}
    @media screen and (max-width: 576px){
        .footerbg {
        position: relative !important;
        bottom: 0;
    }
}
</style>