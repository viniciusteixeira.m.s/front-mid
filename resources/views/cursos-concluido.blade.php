@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid mt-5 pt-5">
 
    <div class="row text-white pt-5 ml-5 ajusttitle loginMobile">
        <div class="col-md-6 col-lg-4 d-none d-xl-block mbdiv">
            <ul class="timeline circle">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>
                </li>
            </ul>
            <ul class="timeline circle">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline lapis">
                <li> 
                    <a class="font font5120">PRIMEIRA AVALIAÇÃO</a>
                    <p class="colorp cortextos">Duração aproximada: 30 minutos</p>                
                </li>
            </ul>
        </div>
            <div class="col-10 col-lg-12 col-xl-6 col-sm-10 pl-sm-4 ajustmob mbdiv">
                <h1 class="text-left font bordersection font-weight-bold fontTermos2 ajusteipad font1200Concluido mb-3"> PARABÉNS!</h1>

                <p class=" titulocursos font colorp cortextos">Mussum Ipsum, cacilds vidis litro abertis. Suco de cevadiss deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
                </p>
                <p class="titulocursos font colorp cortextos">Detraxit consequat et quo num tendi nada. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Paisis, filhis, espiritis santis.
                </p>   
                <p class="titulocursos font colorp cortextos"> Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis. Delegadis gente finis, bibendum egestas augue arcu ut est. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
                </p>
                <p class="titulocursos font colorp cortextos">Mussum Ipsum, cacilds vidis litro abertis. deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
                </p>
                <a href="#" id="link"> deixa as pessoas mais interessantis</a> 
            </div>
           
    </div>
</div>

@endsection

@section('footer')
@endsection

@section('script')
@endsection

<style>
    @media screen and (min-width: 1920px){
        .footerbg{
            position: absolute !important;
        }
    }
</style>