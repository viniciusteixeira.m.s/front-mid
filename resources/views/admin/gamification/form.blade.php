@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">


    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dist/min/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <!-- <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script> -->

    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{asset('js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>
    <script src="{{asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Page JS Code -->
    <!-- <script src="{{ asset('js/pages/tables_datatables.js') }}"></script> -->
    <script>
        jQuery(function(){ Dashmix.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'rangeslider', 'masked-inputs', 'pw-strength']); });
        
        $( function() {
            $( "#sortable1, #sortable2" ).sortable({
            connectWith: ".connectedSortable"
            }).disableSelection();
        } );
    </script>
@endsection

@section('content')
<div class="bg-body-light">
                    <div class="content content-full">
                        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                            <h1 class="flex-sm-fill font-size-h3 font-w400 mt-2 mb-0 mb-sm-2">Nova Gameficação</h1>
                            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Gameficação</li>
                                    <li class="breadcrumb-item active" aria-current="page">Novo</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Layouts -->
                    <div class="block block-rounded block-bordered">
                        <!-- <div class="block-header block-header-default">
                            <h3 class="block-title">Layouts</h3>
                        </div> -->
                        <div class="block-content">
                         
                            <!-- Grid Based Layout -->
                            <h2 class="content-heading">Dados da Gameficação</h2>
                           
                            
                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <!-- Form Grid with Labels -->
                                    <form action="be_forms_layouts.html" method="POST" onsubmit="return false;">
                                        <div class="form-group form-row">
                                            <div class="col-12">
                                                <label>Titulo</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <!-- <div class="col-6">
                                                <label>Parametro</label>
                                                <input type="text" class="form-control">
                                            </div> -->
                                        </div>
                                       
                                        <div class="form-group form-row">
                                            <div class="col-12">
                                      
                                                <label for="example-textarea-input">Descrição</label>
                                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="4" placeholder="Descrição do tema"></textarea>
                                         
                                            </div>
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-12 overflow-hidden">
                                      
                                                <div class="form-group">
                                                    <label class="d-block" for="example-file-input">Foto do badge</label>
                                                    <input type="file" id="example-file-input" name="example-file-input">
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="content-heading">Pontos</h2>
                                        <div class="form-group form-row">
                                            <div class="col-6">
                                                <label>De</label>
                                                <input type="number" class="form-control">
                                            </div>
                                            <div class="col-6">
                                                <label>Até</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>

                                        




                                       



                                        <div class="form-group form-row text-right">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-dark">Cadastrar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Grid with Labels -->
                                </div>
                            </div>
                            <!-- END Grid Based Layout -->
                        </div>
                    </div>
                    <!-- END Layouts -->
                </div>
                <!-- END Page Content -->
                
    <!-- Hero -->
   

                    
               
@endsection
