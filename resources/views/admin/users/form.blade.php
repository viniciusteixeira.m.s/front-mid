@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">


    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dist/min/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <!-- <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script> -->

    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{asset('js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>
    <script src="{{asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>

    <!-- Page JS Code -->
    <!-- <script src="{{ asset('js/pages/tables_datatables.js') }}"></script> -->
    <script>jQuery(function(){ Dashmix.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'rangeslider', 'masked-inputs', 'pw-strength']); });</script>
@endsection

@section('content')
<div class="bg-body-light">
                    <div class="content content-full">
                        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                            <h1 class="flex-sm-fill font-size-h3 font-w400 mt-2 mb-0 mb-sm-2">Novo Usuario</h1>
                            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Usuario</li>
                                    <li class="breadcrumb-item active" aria-current="page">Novo</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Layouts -->
                    <div class="block block-rounded block-bordered">
                        <!-- <div class="block-header block-header-default">
                            <h3 class="block-title">Layouts</h3>
                        </div> -->
                        <div class="block-content">
                         
                            <!-- Grid Based Layout -->
                            <h2 class="content-heading">Dados Obrigatiorios</h2>
                           
                            
                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <!-- Form Grid with Labels -->
                                    <form action="{{ route('user.store')}}" method="POST" onsubmit="return true;">
                                        @csrf
                                        <div class="form-group form-row">
                                            <div class="col-6">
                                                <label>Nome</label>
                                                <input id="name" name="name" type="text" class="form-control">
                                            </div>
                                            <div class="col-6">
                                                <label>E-mail</label>
                                                <input id="email" name="email" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-6">
                                                <label>CPF/CNPJ</label>
                                                <input id="document" name="document" type="text" class="form-control">
                                            </div>
                                            <div class="col-6">
                                                <label>Perfil</label>
                                                <input id="profile" name="profile" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <h2 class="content-heading">Dados Adicionais</h2>
                                        <div class="form-group form-row">
                                            <div class="col-4">
                                                <label>Apelido</label>
                                                <input id="nickname" name="nickname" type="text" class="form-control">
                                            </div>
                                            <div class="col-4">
                                                <label>Grupo</label>
                                                <select id="grupo" name="grupo" class="js-select2 form-control"  style="width: 100%;" data-placeholder="Selecione..">
                                                    <option></option>
                                                    <option value="1">HTML</option>
                                                    <option value="2">CSS</option>
                                                    <option value="3">JavaScript</option>
                                                    <option value="4">PHP</option>
                                                    <option value="5">MySQL</option>
                                                    <option value="6">Ruby</option>
                                                    <option value="7">Angular</option>
                                                    <option value="8">React</option>
                                                    <option value="9">Vue.js</option>
                                             </select>
                                            </div>
                                            
                                            <div class="col-4">
                                                <label>Departamento</label>
                                                <input id="department" name="department" type="text" class="form-control">
                                            </div>
                                           
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-4">
                                                <label>Função</label>
                                                <input id="occupation" name="occupation" type="text" class="form-control">
                                            </div>
                                            <div class="col-4"> 
                                                <label for="birthday">Data de aniversário</label>
                                                <input id="birthday" name="birthday" type="text" class="js-flatpickr form-control bg-white" placeholder="Selecione a data" data-date-format="d/m/Y">
                                            </div>
                                        
                                            <div class="col-4">
                                                <label class="d-block pb-1">Sexo</label>
                                                <div class="form-check form-check-inline">
                                                    <input id="genderM" name="genderM" class="form-check-input" type="radio" value="M" checked="">
                                                    <label class="form-check-label" for="example-radios-inline1">Masculino</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input id="genderF" name="genderF" class="form-check-input" type="radio" value="F">
                                                    <label class="form-check-label" for="example-radios-inline2">Feminino</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-4">
                                                <label>CEP</label>
                                                <input id="cep" name="cep" type="text" class="js-masked-cep form-control" placeholder="0000000-000">
                                            </div>
                                            <div class="col-4">
                                                <label>Bairro</label>
                                                <input id="district" name="district" type="text" class="form-control">
                                            </div>
                                            <div class="col-4">
                                                <label>Cidade</label>
                                                <input id="city" name="city" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-8">
                                                <label>Logradouro</label>
                                                <input id="place" name="place" type="text" class="form-control">
                                            </div>
                                            <div class="col-2">
                                                <label>Número</label>
                                                <input id="number" name="number" type="text" class="form-control">
                                            </div>
                                            <div class="col-2">
                                                <label>UF</label>
                                                <input id="state" name="state" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-row text-right">
                                            <div class="col-sm-12">
                                                <button id="btnCadastrar" name="btnCadastrar" type="submit" class="btn btn-dark">Cadastrar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Grid with Labels -->
                                </div>
                            </div>
                            <!-- END Grid Based Layout -->
                        </div>
                    </div>
                    <!-- END Layouts -->
                </div>
                <!-- END Page Content -->
                
    <!-- Hero -->
   

                    
               
@endsection
