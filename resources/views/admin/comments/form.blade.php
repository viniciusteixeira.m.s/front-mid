@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">


    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dist/min/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <!-- <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script> -->

    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{asset('js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>
    <script src="{{asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>

    <!-- Page JS Code -->
    <!-- <script src="{{ asset('js/pages/tables_datatables.js') }}"></script> -->
    <script>jQuery(function(){ Dashmix.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'rangeslider', 'masked-inputs', 'pw-strength']); });</script>
@endsection

@section('content')
<div class="bg-body-light">
                    <div class="content content-full">
                        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                            <h1 class="flex-sm-fill font-size-h3 font-w400 mt-2 mb-0 mb-sm-2">Novo Grupo</h1>
                            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Grupo</li>
                                    <li class="breadcrumb-item active" aria-current="page">Novo</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Layouts -->
                    <div class="block block-rounded block-bordered">
                        <!-- <div class="block-header block-header-default">
                            <h3 class="block-title">Layouts</h3>
                        </div> -->
                        <div class="block-content">
                         
                            <!-- Grid Based Layout -->
                            <h2 class="content-heading">Dados do Grupo</h2>
                           
                            
                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <!-- Form Grid with Labels -->
                                    <form action="be_forms_layouts.html" method="POST" onsubmit="return false;">
                                        <div class="form-group form-row">
                                            <div class="col-12">
                                                <label>Nome</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-12">
                                      
                                                <label for="example-textarea-input">Descrição</label>
                                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="4" placeholder="Descrição do grupo"></textarea>
                                         
                                            </div>
                                        </div>
                                        <h2 class="content-heading">Usuarios</h2>
                                       
                                        <div class="block block-rounded block-bordered">
                                        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                            <li class="nav-item">
                                            <a class="nav-link active" href="#search-users">Usuários cadastrado</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#search-users-ok">Usuários disponíveis para cadastro</a>
                                            </li>
                                        </ul>
                        <div class="block-content tab-content overflow-hidden">
                            <!-- Classic -->
                                    

                                    <!-- Users -->
                                    <div class="tab-pane fade show active" id="search-users" role="tabpanel">
                                        <!-- <div class="font-size-h3 font-w600 pt-2 pb-4 mb-4 text-center border-bottom">
                                            <span class="text-primary font-w700">10</span> Resultados encont <mark class="text-danger">client</mark>
                                        </div> -->
                                        <table class="table table-striped table-borderless table-vcenter">
                                            <thead class="thead-light">
                                                <tr>
                                                    <!-- <th class="d-none d-sm-table-cell text-center" style="width: 40px;">#</th> -->
                                                    <th class="text-center" style="width: 70px;"><i class="si si-user"></i></th>
                                                    <th>Name</th>
                                                    <th class="d-none d-sm-table-cell">Email</th>
                                                    <th class="d-none d-lg-table-cell" style="width: 15%;">Access</th>
                                                    <th class="text-center" style="width: 80px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                   
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar10.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Jose Wagner</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client1@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-warning">Trial</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar3.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Marie Duncan</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client2@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-success">VIP</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar13.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Jose Wagner</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client3@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-primary">Personal</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                  
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar16.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Wayne Garcia</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client4@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-danger">Disabled</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                        <nav aria-label="Users Search Navigation">
                                            <ul class="pagination">
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)" tabindex="-1" aria-label="Previous">
                                                        <span aria-hidden="true">
                                                            <i class="fa fa-angle-double-left"></i>
                                                        </span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active">
                                                    <a class="page-link" href="javascript:void(0)">1</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)">4</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)" aria-label="Next">
                                                        <span aria-hidden="true">
                                                            <i class="fa fa-angle-double-right"></i>
                                                        </span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <!-- END Users -->

                                      <!-- Users -->
                                      <div class="tab-pane fade" id="search-users-ok" role="tabpanel">
                                        <!-- <div class="font-size-h3 font-w600 pt-2 pb-4 mb-4 text-center border-bottom">
                                            <span class="text-primary font-w700">10</span> results found for <mark class="text-danger">client</mark>
                                        </div> -->
                                        <table class="table table-striped table-borderless table-vcenter">
                                            <thead class="thead-light">
                                                <tr>
                                                    <!-- <th class="d-none d-sm-table-cell text-center" style="width: 40px;">#</th> -->
                                                    <th class="text-center" style="width: 70px;"><i class="si si-user"></i></th>
                                                    <th>Name</th>
                                                    <th class="d-none d-sm-table-cell">Email</th>
                                                    <th class="d-none d-lg-table-cell" style="width: 15%;">Access</th>
                                                    <th class="text-center" style="width: 80px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">1</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar10.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Jose Wagner</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client1@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-warning">Trial</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">2</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar3.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Marie Duncan</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client2@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-success">VIP</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">3</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar13.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Jose Wagner</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client3@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-primary">Personal</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">4</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar16.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Wayne Garcia</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client4@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-danger">Disabled</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">5</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Albert Ray</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client5@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-success">VIP</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">6</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar8.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Andrea Gardner</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client6@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-danger">Disabled</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">7</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Henry Harrison</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client7@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-primary">Personal</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">8</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar7.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Judy Ford</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client8@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-success">VIP</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">9</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar16.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Thomas Riley</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client9@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-warning">Trial</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!-- <td class="d-none d-sm-table-cell text-center">
                                                        <span class="badge badge-pill badge-primary">10</span>
                                                    </td> -->
                                                    <td class="text-center">
                                                        <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar2.jpg') }}" alt="">
                                                    </td>
                                                    <td class="font-w600">
                                                        <a href="javascript:void(0)">Susan Day</a>
                                                    </td>
                                                    <td class="d-none d-sm-table-cell">
                                                        client10@example.com
                                                    </td>
                                                    <td class="d-none d-lg-table-cell">
                                                        <span class="badge badge-info">Business</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <nav aria-label="Users Search Navigation">
                                            <ul class="pagination">
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)" tabindex="-1" aria-label="Previous">
                                                        <span aria-hidden="true">
                                                            <i class="fa fa-angle-double-left"></i>
                                                        </span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item active">
                                                    <a class="page-link" href="javascript:void(0)">1</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)">3</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)">4</a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="javascript:void(0)" aria-label="Next">
                                                        <span aria-hidden="true">
                                                            <i class="fa fa-angle-double-right"></i>
                                                        </span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <!-- END Users -->
                                   
                                </div>
                            </div>








                                        <div class="form-group form-row text-right">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-dark">Cadastrar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Grid with Labels -->
                                </div>
                            </div>
                            <!-- END Grid Based Layout -->
                        </div>
                    </div>
                    <!-- END Layouts -->
                </div>
                <!-- END Page Content -->
                
    <!-- Hero -->
   

                    
               
@endsection
