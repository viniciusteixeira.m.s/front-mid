@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">


    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dist/min/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">


@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <!-- <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.colVis.min.js') }}"></script> -->

    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{asset('js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>
    <script src="{{asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Page JS Code -->
    <!-- <script src="{{ asset('js/pages/tables_datatables.js') }}"></script> -->
    <script>
        jQuery(function(){ Dashmix.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'rangeslider', 'masked-inputs', 'pw-strength']); });
        
        $( function() {
            $( "#sortable1, #sortable2" ).sortable({
            connectWith: ".connectedSortable"
            }).disableSelection();
        } );
    </script>
@endsection

@section('content')
<div class="bg-body-light">
                    <div class="content content-full">
                        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                            <h1 class="flex-sm-fill font-size-h3 font-w400 mt-2 mb-0 mb-sm-2">Novo Tema</h1>
                            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Tema</li>
                                    <li class="breadcrumb-item active" aria-current="page">Novo</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Layouts -->
                    <div class="block block-rounded block-bordered">
                        <!-- <div class="block-header block-header-default">
                            <h3 class="block-title">Layouts</h3>
                        </div> -->
                        <div class="block-content">
                         
                            <!-- Grid Based Layout -->
                            <h2 class="content-heading">Dados do Tema</h2>
                           
                            
                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <!-- Form Grid with Labels -->
                                    <form action="be_forms_layouts.html" method="POST" onsubmit="return false;">
                                        <div class="form-group form-row">
                                            <div class="col-6">
                                                <label>Nome</label>
                                                <input type="text" class="form-control">
                                            </div>
                                            <div class="col-6">
                                                <label>Instrutor/ Facilitador</label>
                                                    <select class="js-select2 form-control" id="facilitador" name="facilitador" style="width: 100%;" data-placeholder="Selecione..">
                                                        <option></option>
                                                        <option value="1">Instrutro 1 </option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group form-row">
                                            <div class="col-12">
                                      
                                                <label for="example-textarea-input">Descrição</label>
                                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="4" placeholder="Descrição do tema"></textarea>
                                         
                                            </div>
                                        </div>
                                        <div class="form-group form-row">
                                            <div class="col-12">
                                      
                                                <label for="example-textarea-input">Pré-requisito</label>
                                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="4" placeholder=""></textarea>
                                         
                                            </div>
                                        </div>

                                        <div class="form-group form-row">
                                            <div class="col-12">
                                                <label>Tags</label>
                                                    <select class="js-select2 form-control" id="grupo" name="grupo" style="width: 100%;" data-placeholder="Selecione..">
                                                        <option></option>
                                                        <option value="1">HTML</option>
                                                </select>
                                            </div>
                                            <!-- <div class="col-6">
                                                <label>Selecione a mídia</label>
                                                    <select class="js-select2 form-control" id="midia" name="midia" style="width: 100%;" data-placeholder="Selecione..">
                                                        <option></option>
                                                        <option value="1">Vídeo</option>
                                                        <option value="2">Anexo</option>
                                                        <option value="3">Artigo</option>
                                                        <option value="4">Infográfico</option>
                                                        <option value="5">Quiz</option>
                                                        <option value="9">Áudio (Podcast)</option>
                                                        <option value="10">Avaliação de Reação</option>
                                                        <option value="11">Assessment</option>
                                                        <option value="12">6D's</option>
                                                </select>
                                            </div> -->
                                        </div>
                                        




                                        <div class="block block-rounded block-bordered">
                                        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                            <li class="nav-item">
                                            <a class="nav-link active" href="#preparacao">Objetos</a>
                                            </li>
                                            <!-- <li class="nav-item">
                                                <a class="nav-link" href="#aplicacao">Aplicação</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#transferencia">Transferência</a>
                                            </li> -->
                                        </ul>
                                                <div class="block-content tab-content overflow-hidden">
                                                <!-- Classic -->
                                                    
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <ul id="sortable1" class="connectedSortable">
                                                            
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-6">
                                                            <ul id="sortable2" class="connectedSortable">
                                                            <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="ui-state-default">
                                                                    <div class="col-xs-12 pd-0 object-cel" media="3" id="75" lock="0" disponivel-de="<!--DD-->" disponivel-ate="<!--DA-->">
                                                                        <div class="pull-left">
                                                                            <i class="nav-main-link-icon si si-arrow-left"></i>
                                                                            <i class="nav-main-link-icon si si-eye"></i>&nbsp;
                                                                            <i class="nav-main-link-icon si si-lock"></i>
                                                                            <span class="iconsmind icon-Newspaper txt-light-gray"></span>
                                                                            <span class="txt-light-gray ft-exo">&nbsp;&nbsp;Atividade</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- Users -->
                                                    
                                                    
                                                   
                                                                                                    
                                                     <div class="tab-pane fade" id="aplicacao" role="tabpanel"> </div>
                                                     <div class="tab-pane fade" id="transferencia" role="tabpanel"> </div>



                                        <div class="form-group form-row text-right">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-dark">Cadastrar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Grid with Labels -->
                                </div>
                            </div>
                            <!-- END Grid Based Layout -->
                        </div>
                    </div>
                    <!-- END Layouts -->
                </div>
                <!-- END Page Content -->
                
    <!-- Hero -->
   

                    
               
@endsection
