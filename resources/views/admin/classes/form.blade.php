@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}">


    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dist/min/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">


@endsection

@section('js_after')
  
    <script src="{{asset('js/pages/be_forms_wizard.js')}}"></script>


    <script src="{{asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>

    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{asset('js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{asset('js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>
    <script src="{{asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>

    <!-- Page JS Code -->
    <!-- <script src="{{ asset('js/pages/tables_datatables.js') }}"></script> -->
    <script>jQuery(function(){ Dashmix.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'rangeslider', 'masked-inputs', 'pw-strength']); });</script>
@endsection

@section('content')
<div class="bg-body-light">
                    <div class="content content-full">
                        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                            <h1 class="flex-sm-fill font-size-h3 font-w400 mt-2 mb-0 mb-sm-2">Novo Objeto</h1>
                            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Objeto</li>
                                    <li class="breadcrumb-item active" aria-current="page">Novo</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- END Hero -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Layouts -->
                    <div class="block block-rounded block-bordered">
                        <!-- <div class="block-header block-header-default">
                            <h3 class="block-title">Layouts</h3>
                        </div> -->
                        <div class="block-content">
                         
                            <!-- Grid Based Layout -->
                            <h2 class="content-heading">Dados da turma</h2>
                    <div class="row">
                      
                        <div class="col-md-12">
                            <!-- Progress Wizard 2 -->
                            <div class="js-wizard-simple block block block-rounded block-bordered">
                                <!-- Wizard Progress Bar -->
                                <div class="progress rounded-0" data-wizard="progress" style="height: 8px;">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 30%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <!-- END Wizard Progress Bar -->

                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-justified" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-progress2-step1" data-toggle="tab">1. CONFIGURAÇÃO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-progress2-step2" data-toggle="tab">2. TEMA</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-progress2-step3" data-toggle="tab">3. PARTICIPANTES</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-progress2-step4" data-toggle="tab">4. MENSAGERIA</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-progress2-step5" data-toggle="tab">5. RESUMO</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form action="be_forms_wizard.html" method="POST">
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 312px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-progress2-step1" role="tabpanel">
                                            <div class="form-group">
                                                <label for="wizard-progress2-firstname">First Name</label>
                                                <input class="form-control form-control-alt" type="text" id="wizard-progress2-firstname" name="wizard-progress2-firstname">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-progress2-lastname">Last Name</label>
                                                <input class="form-control form-control-alt" type="text" id="wizard-progress2-lastname" name="wizard-progress2-lastname">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-progress2-email">Email</label>
                                                <input class="form-control form-control-alt" type="email" id="wizard-progress2-email" name="wizard-progress2-email">
                                            </div>
                                        </div>
                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                        <div class="tab-pane" id="wizard-progress2-step2" role="tabpanel">
                                            <div class="form-group">
                                                <label for="wizard-progress2-bio">Bio</label>
                                                <textarea class="form-control form-control-alt" id="wizard-progress2-bio" name="wizard-progress2-bio" rows="7"></textarea>
                                            </div>
                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane" id="wizard-progress2-step3" role="tabpanel">
                                            <div class="form-group">
                                                <label for="wizard-simple2-location">Location</label>
                                                <input class="form-control form-control-alt" type="text" id="wizard-progress2-location" name="wizard-simple2-location">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-progress2-skills">Skills</label>
                                                <select class="form-control form-control-alt" id="wizard-progress2-skills" name="wizard-progress2-skills">
                                                    <option value="">Please select your best skill</option>
                                                    <option value="1">Photoshop</option>
                                                    <option value="2">HTML</option>
                                                    <option value="3">CSS</option>
                                                    <option value="4">JavaScript</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox custom-control-primary">
                                                    <input type="checkbox" class="custom-control-input" id="wizard-progress2-terms" name="wizard-progress2-terms">
                                                    <label class="custom-control-label" for="wizard-progress2-terms">Agree with the terms</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="wizard-progress2-step4" role="tabpanel"></div>
                                        <div class="tab-pane" id="wizard-progress2-step5" role="tabpanel"></div>

                                        <!-- END Step 3 -->
                                    </div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-1"></i> Previous
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-secondary" data-wizard="next">
                                                    Next <i class="fa fa-angle-right ml-1"></i>
                                                </button>
                                                <button type="submit" class="btn btn-primary d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-1"></i> Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
                            <!-- END Progress Wizard 2 -->
                        </div>
                    </div>

                            
                          
                            <!-- END Grid Based Layout -->
                        </div>
                    </div>
                    <!-- END Layouts -->
                </div>
                <!-- END Page Content -->
                
    <!-- Hero -->
   

                    
               
@endsection
