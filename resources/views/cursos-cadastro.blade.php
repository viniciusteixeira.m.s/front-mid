@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid d-none d-xl-block">
    <div class="row">
        <div class="col-lg-7 offset-lg-1 mbdiv">
        <form method="POST">
                        <h1 class="text-left text-white font-weight-bold font text-uppercase contatosobre ajustetitulo mb-4">Faça seu cadastro</h1>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="name" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Nome</label>
                            <div class="col-md-8 col-lg-8 font">
                                <input id="name" type="name" class="font form-control @error('E-mail') is-invalid @enderror" name="name"  autofocus style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="lastname" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Sobrenome</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="lastname" type="lastname" class="font form-control" name="lastname" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="cpf" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">CPF</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="cpf" type="cpf" class="font form-control" name="cpf" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="genero" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Gênero</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="genero" type="genero" class="font form-control" name="genero" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 mrCursosCadastro1200">
                                <div class="form-group row" style="display: flow-root ">
                                    <label for="city" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Cidade</label>
                                        <input id="city" type="text" class="font form-control" name="city" style="border-radius: 0; margin-left: 15px">
                                            <span class="invalid-feedback" role="alert">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="col-3 mlCursosCadastro1200">
                                <div class="form-group row" style="display: flow-root ">
                                    <label for="state" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Estado</label>
                                        <input id="state" type="text" class="font form-control" name="state" style="border-radius: 0">
                                            <span class="invalid-feedback" role="alert">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="email" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Endereço</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="address" type="address" class="font form-control" name="address" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="email" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Email</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="email" type="email" class="font form-control" name="email" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="password" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Senha</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="password" type="password" class="font form-control" name="password" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="password2" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Confirmar Senha</label>
                            <div class="col-md-8 col-lg-8">
                                <input id="password2" type="password" class="font form-control" name="password2" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                            <div class="col-md-4 col-lg-8 text-right mt-5 cadastromb">
                                <button type="submit font" class="btn btn-outline-warning btnenviar">ENVIAR
                                </button>
                            </div>
                        </div>
        </form>
    </div>
</div>
<div class="container-fluid d-block d-xl-none">
    <div class="row">
        <div class="col-xl-7 col-lg-12 offset-xl-1">
        <form method="POST">
                        <h1 class="text-left text-white font-weight-bold font text-uppercase contatosobre ajustetitulo mb-4">Faça seu cadastro</h1>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="name" type="name" class="font form-control @error('E-mail') is-invalid @enderror" name="name" placeholder="Nome"  autofocus style="border-radius: 0">

                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="lastname" type="lastname" class="font form-control" placeholder="Sobrenome" name="lastname" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="cpf" type="cpf" class="font form-control" name="cpf" placeholder="CPF" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="genero" type="genero" class="font form-control" placeholder="Gênero" name="genero" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-7 mr-2 ml768cadastro">
                                <div class="form-group row" style="display: flow-root ">
                                        <input id="city" type="text" class="font form-control" name="city" placeholder="Cidade" style="border-radius: 0; margin-left: 15px">
                                            <span class="invalid-feedback" role="alert">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                            <div class="col-sm-5 col-md-4 ml-4">
                                <div class="form-group row" style="display: flow-root ">
                                        <input id="state" type="text" class="font form-control" name="state" placeholder="Estado" style="border-radius: 0">
                                            <span class="invalid-feedback" role="alert">
                                                <strong></strong>
                                            </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="address" type="address" class="font form-control" name="address" placeholder="Endereço" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="email" type="email" class="font form-control" name="email" placeholder="Email" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="password" type="password" class="font form-control" placeholder="Senha" name="password" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <div class="col-sm-12 col-md-12 font">
                                <input id="password2" type="password" class="font form-control" placeholder="Confirmar Senha" name="password2" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8 text-right mt-5 cadastromb">
                                <button type="submit font" class="btn btn-outline-warning btnenviar">ENVIAR
                                </button>
                            </div>
                        </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')
@endsection
