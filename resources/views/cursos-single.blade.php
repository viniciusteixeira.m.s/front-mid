@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid pr-0 pl-0">
    <img src="{{ asset('images/banner-2.jpg') }}" class="bgbanner1 d-none d-xl-block"/>
    <img src="{{ asset('images/single992.jpg') }}" class="bgbanner1 d-none d-lg-block d-xl-none"/>
    <img src="{{ asset('images/CURSO_SINGLE.jpg') }}" class="bgbanner1 d-block d-md-none"/>
    <img src="{{ asset('images/single768.jpg') }}" class="bgbanner1 d-none d-md-block d-lg-none"/>
</div>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-lg-12">
            <h1 class="text-center font titlesingle"> TÍTULO DO CURSO</h1>
        </div>
        <p class="col-lg-12  text-center subtitlesingle">Professor do Curso</p>
        <div class="col-lg-9">
            <p class="text-center psingle mt-4">Mussum Ipsum, cacilds vidis litro abertis. Viva Forevis aptent taciti sociosqu ad litora torquent. Delegadis gente finis, bibendum egestas augue arcu ut est. 
                Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Leite de capivaris, leite de mula manquis sem cabeça.
            </p>
            <p class="text-center psingle">Mussum Ipsum, cacilds vidis litro abertis. Viva Forevis aptent taciti sociosqu ad litora torquent. Delegadis gente finis, bibendum egestas augue arcu ut est. 
                Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Leite de capivaris, leite de mula manquis sem cabeça.
            </p>
            <p class="text-center psingle">Mussum Ipsum, cacilds vidis litro abertis. Viva Forevis aptent taciti sociosqu ad litora torquent. Delegadis gente finis, bibendum egestas augue arcu ut est. 
                Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Leite de capivaris, leite de mula manquis sem cabeça.
            </p>
            <p class="text-center psingle">Mussum Ipsum, cacilds vidis litro abertis. Viva Forevis aptent taciti sociosqu ad litora torquent. Delegadis gente finis, bibendum egestas augue arcu ut est. 
                Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Leite de capivaris, leite de mula manquis sem cabeça.
            </p>
            <div class="text-center">
                <button type="button" class="btn btn-outline-warning btnenviar mt-4 font btnsingle mt-5">COMECE AGORA MESMO</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt-5 container1920">
    <div class="row justify-content-center">
        <img src="{{ asset('images/cursos-single2.jpeg') }}" class="bgbanner1 mt-5 pt-5 img1single d-none d-xl-block"/>
        <img src="{{ asset('images/cursos-single2.jpeg') }}" class="bgbanner1 mt-5 pt-5 img1single d-none d-sm-block d-md-none"/>
        <img src="{{ asset('images/cursos-single2.jpeg') }}" class="bgbanner1 mt-5 pt-5 img1single d-none d-lg-block d-xl-none"/>
        <img src="{{ asset('images/singleVideo768.jpg') }}" class="bgbanner1 mt-5 pt-5 img1single d-none d-md-block d-lg-none"/>

        <div class="justify-content-center"><img src="{{ asset('images/play.svg') }}" class="bgbanner1 img2single"/></div>
    </div>
</div>
<div class="container-fluid container1920">
    <div class="row">
        <h1 class="text-left font titlecollapse mb-5 mtSingleMob"> CONTEÚDO DO CURSO</h1>
        <div class="accordion sizecollapse" id="accordionExample">
            <div class="card mbcollapse">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                  <button class="collapsible btn textcollapse font itemtitle" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    TÍTULO DA AULA EM DESTAQUE
                  </button>
                </h2>
              </div>
          
              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="internaltext font">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="card mbcollapse">
              <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                  <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    TÍTULO DA AULA EM DESTAQUE
                  </button>
                </h2>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="internaltext font">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="card mbcollapse">
              <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                  <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    TÍTULO DA AULA EM DESTAQUE
                  </button>
                </h2>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="internaltext font">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="card mbcollapse">
                <div class="card-header" id="headingFour">
                  <h2 class="mb-0">
                    <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                      TÍTULO DA AULA EM DESTAQUE
                    </button>
                  </h2>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                  <div class="internaltext font">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card mbcollapse">
                <div class="card-header" id="headingFive">
                  <h2 class="mb-0">
                    <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                      TÍTULO DA AULA EM DESTAQUE
                    </button>
                  </h2>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <div class="internaltext font">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card mbcollapse">
                <div class="card-header" id="headingSix">
                  <h2 class="mb-0">
                    <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                      TÍTULO DA AULA EM DESTAQUE
                    </button>
                  </h2>
                </div>
                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                  <div class="internaltext font">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card mbcollapse">
                <div class="card-header" id="headingSeven">
                  <h2 class="mb-0">
                    <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
                      TÍTULO DA AULA EM DESTAQUE
                    </button>
                  </h2>
                </div>
                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                  <div class="internaltext font">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card mbcollapse">
                <div class="card-header" id="headingEight">
                  <h2 class="mb-0">
                    <button class="collapsible btn textcollapse collapsed font itemtitle" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
                      TÍTULO DA AULA EM DESTAQUE
                    </button>
                  </h2>
                </div>
                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                  <div class="internaltext font">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
          </div>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')

@endsection
