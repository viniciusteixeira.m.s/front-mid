@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid pr-0 pl-0 pb-5 mb-5 pb-sm-0">
    <img src="{{ asset('images/banner-1.jpg') }}" class="bgbanner1 d-none d-xl-block"/>
    <img src="{{ asset('images/home.jpg') }}" class="bgbanner1 d-block d-md-none"/>
    <img src="{{ asset('images/home992.jpg') }}" class="bgbanner1 d-none d-lg-block d-xl-none"/>
    <img src="{{ asset('images/home768.jpg') }}" class="bgbanner1 d-none d-md-block d-lg-none"/>

                        {{-- ajuste breakpoint 1920--}}
        <div class=" d-none d-xl-block row-d-flex justify-content-center position1920 position1200 position5120">
            <div class="d-none d-xl-block col-lg-3 text-left offset-lg-7 h1btn1920 h1btn1200">
                <h1 class="text-white font-weight-bold font h1btn1920 h1btn1200 h1Index5120"> QUEM É A<br/> ESTILO MID?</h1>
                <button type="button" class="btn btn-outline-warning mt-4 font btnindex">CONHEÇA A NOSSA HISTÓRIA</button>
            </div>
            <div class="col-2 play1920 d-none d-md-block offset-lg-5 play1920 play1200 play5120">
                <img src="{{ asset('images/play.svg') }}" style="width: 60%" class="bgbanner1"/>
            </div>
        </div>
                        {{-- ajuste breakpoint 576--}}
        <div class="d-lg-block d-xl-none position576 position768 mb-5 pb-5">
                <div class="col-lg-12 col-sm-12 text-center">
                    <img src="{{ asset('images/play.svg') }}" style="width: 23%; display: inline-table; opacity: 0.5" class="bgbanner1"/>
                </div>
         
            <div class="col-lg-12 col-sm-12 text-center mt-5 pt-5 ajusteIndex768">
                <h1 class="text-white font-weight-bold font mt-5 pt-5 tituloIndexH1"> QUEM É A<br/> ESTILO MID?</h1>
                <button type="button" class="btn btn-outline-warning mt-4 font btnindex">CONHEÇA A NOSSA HISTÓRIA</button>
            </div>
        </div>
</div>

<h1 class="bordersection text-center pt-5 mb-5 font mtindex h1cursosIndex5120"> CURSOS DISPONÍVEIS</h1>
    <div class="container-fluid pr-0 pl-0">
        <img src="{{ asset('images/banner-2.jpg') }}" class="bgbanner1 d-none d-xl-block"/>
        <img src="{{ asset('images/carousel.jpeg') }}" class="bgbanner1 d-block d-md-none"/>
        <img src="{{ asset('images/carousel.jpeg') }}" class="bgbanner1 d-none d-md-block d-lg-none"/>
        <img src="{{ asset('images/carousel.jpeg') }}" class="bgbanner1 d-none d-lg-block d-xl-none"/>
        <div class="row offset-xl-1">
            <h1 class="text-white text-left ml-5 font d-lg-block d-xl-none textmobile text768"> TÍTULO DO CURSO | Professor do Curso</h1>
        </div>
    </div>

<div class="container-fluid d-none d-xl-block">
    <div class="row offset-lg-1">
        <h1 class="text-white text-left ml-5 font d-none d-md-block h1cursosIndex5120"> TÍTULO DO CURSO | Professor do Curso</h1>
        <h1 class="text-white text-left ml-5 font d-block d-md-none textmobile"> TÍTULO DO CURSO | Professor do Curso</h1>

    </div>
    <div id="carouselExampleControls d-none d-md-block" class="carousel slide mb-5 pb-5 mt-5 pt-5" data-ride="carousel">
        <div class="row mb-5 pb-5">
            <div class="col-4 text-right">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/slider-1.jpg') }}" class="ajusteSlider1200 imgSliderIndex5120"/>
                        
                    </div>
                </div>
            </div>
            <div class="col-4 text-center">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/slider-2.jpg') }}" class="ajusteSlider1200 imgSliderIndex5120"/>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/slider-3.jpg') }}" class="ajusteSlider1200 imgSliderIndex5120"/>
                    </div>
                </div>
            </div>
        </div>
            <a class="carousel-control-prev setasIndex" role="button" data-slide="prev">
                {{-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> --}}
                <img src="{{ asset('images/seta_ant.png') }}" class="setas5120"  aria-hidden="true" />
            </a>
            <a class="carousel-control-next setasIndex" role="button" data-slide="next">
                {{-- <span class="carousel-control-next-icon" aria-hidden="true"></span> --}}
                <img src="{{ asset('images/seta-prox.png') }}" class="setas5120"  aria-hidden="true"/>
            </a>
        </div>
    </div>
    <div class="container-fluid ajuste5120Index">
        <div class="row mt-5 pt-5 mb-5 pb-5 ml-lg-2">
            <div class="col-xl-5 col-lg-12 offset-xl-1 col-sm-12">
                <h5 class="coltext font d-none d-md-block h5Index5120"> Saiba de nossos <br/>novos cursos antes de todo mundo</h5>
                <h5 class="coltext font d-block d-md-none"> Saiba de nossos novos  <br/>cursos antes de todo mundo</h5>

                <span class="coltext2 font spanIndex5120">Inscreva-se em nossa newsletter e fique por dentro <br/> de todos os nossos cursos.</span>
            </div>
            <div class="col-xl-5 col-lg-12 col-sm-12 mr-lg-5 mt-3">
                <input class="form-control mbbtnIndex5120" style="border-radius: 0">
                <div class="text-right mt-3">
                </div>
                <div class="text-right">
                 <button type="button" class="btn btn-outline-warning btnenviar font">ENVIAR</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid mtindex" >
    <h1 class="bordersection text-center mt-5 pt-5 font d-none d-md-block fsTitulo768 h1cursosIndex5120 h1Alunos5120"> O QUE DIZEM NOSSOS ALUNOS?</h1>
    <h1 class="bordersection text-center mt-5 pt-5 font d-block d-md-none titleindex"> O QUE DIZEM NOSSOS ALUNOS?</h1>
    <div id="carouselExampleControls" class="carousel slide mb-5 pb-5 mt-5 pt-5" data-ride="carousel">
        <div class="row">
            <div class="col-xl-4 col-lg-10 col-sm-10 offset-lg-2 offset-sm-1 offset-md-2">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/img_chapeu.png') }}" style="width: 60%"/>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-5 col-sm-6 col-7 col-md-5 bordaFoto text-left offset-lg-2">
                    <p class="bordaTexto font">"Encontrar uma forma<br> de falar de estilo de uma<br> forma mais profunda, <br>mais conectada com as<br> pessoas, suas <br>necessidades e <br/>objetivos."</p>
                    <p class="bordaTexto2 font">Lais Lima, 23 anos</p>
                </div>
            </div>
            <div class="col-lg-4 col-xl-4 offset-1 d-none d-xl-block">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('images/img_chapeu.png')}}" width="60%"/>
                    </div>
                </div>
                <div class="col-5 col-xl-6 bordaFoto text-left offset-2">
                    <p class="bordaTexto font">"Encontrar uma forma<br> de falar de estilo de uma<br> forma mais profunda, <br>mais conectada com as<br> pessoas, suas <br>necessidades e <br/>objetivos."</p>
                    <p class="bordaTexto2 mb-4 font">Lais Lima, 23 anos</p>
                </div>
            </div>
        </div>
            <a class="carousel-control-prev" role="button" data-slide="prev">
                {{-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> --}}
                <img src="{{ asset('images/seta_ant.png') }}" class="setas5120" aria-hidden="true" />
            </a>
            <a class="carousel-control-next" role="button" data-slide="next">
                {{-- <span class="carousel-control-next-icon" aria-hidden="true"></span> --}}
                <img src="{{ asset('images/seta-prox.png') }}" class="setas5120" aria-hidden="true"/>
            </a>
        </div>
    </div>
  
    

@endsection 

@section('footer')
@endsection

@section('script')

@endsection

