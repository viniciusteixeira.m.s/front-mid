@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid mt-5 pt-5">
 
    <div class="row text-white pt-4 offset-lg-1 ajusttitle">
        <div class="col-md-6 col-lg-4 d-none d-lg-block mbdiv">
            <ul class="timeline circle">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="cortextos colorp">Introdução ao curso - Leitura de 3 minutos.</p>
                </li>
            </ul>
            <ul class="timeline circle">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="cortextos colorp">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="cortextos colorp">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline bordersection">
                <li>
                    <a class="bordersection font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="bordersection colorp">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font bordersection font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="cortextos colorp">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="lapis timeline">
                <li>
                    <a class="font font5120">PRIMEIRA AVALIAÇÃO</a>
                    <p class="cortextos colorp">Duração aproximada: 30 minutos</p>                
                </li>
            </ul>
        </div>
            <div class="col-10 col-lg-6 col-sm-12 pl-sm-4 ajustmob mbdiv cursosAula1200">
                <img src="{{ asset('images/cursos-single2.jpeg') }}" class="bgbanner1 pt-5 img1single"/>
                <div class="justify-content-center">
                    <img src="{{ asset('images/play.svg') }}" class="bgbanner1 img1920cursoaula"/>
                </div>
                <h1 class="text-left font bordersection font-weight-bold mt-5 introtitle"> INTRODUÇÃO AO CURSO</h1>

                <p class="font cortextos">Mussum Ipsum, cacilds vidis litro abertis. Suco de cevadiss deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
                </p>
                <p class="font cortextos">Detraxit consequat et quo num tendi nada. Manduma pindureta quium dia nois paga. Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo! Paisis, filhis, espiritis santis.
                </p>   
                <p class="font cortextos"> Admodum accumsan disputationi eu sit. Vide electram sadipscing et per. Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis. Delegadis gente finis, bibendum egestas augue arcu ut est. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
                </p>
                <p class="font cortextos mb-4">Mussum Ipsum, cacilds vidis litro abertis. deixa as pessoas mais interessantis. Quem num gosta di mé, boa gentis num é. Copo furadis é disculpa de bebadis, arcu quam euismod magna. Quem num gosta di mim que vai caçá sua turmis!
                </p>
                <a href="#" id="link" class="mt-5 linkCursosAula"> Clique e baixe o material da aula</a> 
            </div>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')
@endsection