@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid pr-0 pl-0">
    <img src="{{ asset('images/banner-sobre.jpg') }}" class="bgbanner1 d-none d-xl-block"/>
    <img src="{{ asset('images/home992.jpg') }}" class="bgbanner1 d-none d-lg-block d-xl-none"/>
    <img src="{{ asset('images/sobre.jpg') }}" class="bgbanner1 d-block d-md-none"/>
    <img src="{{ asset('images/home768.jpg') }}" class="bgbanner1 d-none d-md-block d-lg-none"/>
        <div class="row d-flex justify-content-center positionSobre">
            <div class="col-2 d-none d-xl-block text-center playsobre">
                <img src="{{ asset('images/play.svg') }}" class="align-center" style="width: 50%;" class="bgbanner1"/>
            </div>
            <div class="col-10 text-center d-lg-block d-xl-none playsobre576 offset-sm-1" style="opacity: 0.55;">
                <img src="{{ asset('images/play.svg') }}" class="align-center" style="width: 28%;" class="bgbanner1"/>
            </div>
        </div>
</div>

<div class="container ajustemob">
    <div class="row sobremb">
        <h1 class="bordersection col-12 mb-3 titlesobre">CLÉLIA MIDORI </h1>
        <div class="col-lg-12 col-xl-5 mr-5">
            <p class=" textsobre">Mussum Ipsum, cacilds vidis litro abertis. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. 
            Per aumento de cachacis, eu reclamis. Diuretics paradis num copo é motivis de denguis. Casamentiss faiz malandris se pirulitá.</p>
            <p class="textsobre">Paisis, filhis, espiritis santis. Si num tem leite então bota uma pinga aí cumpadi!
            In elementis mé pra quem é amistosis quis leo. Mé faiz elementum girarzis, nisi eros vermeio.</p>
            <p class="textsobre">Paisis, filhis, espiritis santis. Si num tem leite então bota uma pinga aí cumpadi!
            In elementis mé pra quem é amistosis quis leo. Mé faiz elementum girarzis, nisi eros vermeio.</p>
            
        </div>
        <div class="col-xl-5 d-lg-none d-xl-block">
            <p class="textsobre">Mussum Ipsum, cacilds vidis litro abertis. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. 
            Per aumento de cachacis, eu reclamis. Diuretics paradis num copo é motivis de denguis. Casamentiss faiz malandris se pirulitá.</p>
            <p class="textsobre">Paisis, filhis, espiritis santis. Si num tem leite então bota uma pinga aí cumpadi!
            In elementis mé pra quem é amistosis quis leo. Mé faiz elementum girarzis, nisi eros vermeio.</p>
            <p class="textsobre">Paisis, filhis, espiritis santis. Si num tem leite então bota uma pinga aí cumpadi!
            In elementis mé pra quem é amistosis quis leo. Mé faiz elementum girarzis, nisi eros vermeio.</p>
        </div>
    </div>
</div>
@endsection

@section('footer')
@endsection

@section('script')

@endsection