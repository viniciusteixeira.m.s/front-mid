<footer>
    <!-- Footer -->
		<div class="footer-full-width pt-5 footerbg headerposition" id="footer">
			<div class="container">
				<div class="row border-footer">
                        <div class="col-6 col-sm-4 pl-0">
                            <div class="title-block text-left ">
                                <ul class="socials mt-4 pl-0 font">
                                    <li>
                                        <p class="pfooter font">Sobre nós</p>
                                    </li>
                                    <li>
                                        <p class="pfooter font">baixe o App</p>
                                    </li>
                                    <li>
                                        <p class="pfooter font">Seja um Professor</p>
                                    </li>
                                    <li class="d-block d-sm-none">
                                        <p class="pfooter font">Blog</p>
                                    </li>
                                </ul>
                            </div>
                         </div>
                         <div class="col-6 col-sm-4">
                            <div class="title-block text-left">
                                <ul class="socials mt-4">
                                    <li>
                                        <p class="pfooter font">Termos de Uso</p>
                                    </li>
                                    <li>
                                        <p class="pfooter font">Política de Privacidade</p>
                                    </li>
                                    <li>
                                        <p class="pfooter font">Suporte</p>
                                    </li>
                                    <li class="d-block d-sm-none">
                                        <p class="pfooter font">Cursos Populares</p>
                                    </li>
                                </ul>
                            </div>
                         </div>
                         <div class="col-6 col-sm-4 d-none d-sm-block">
                            <div class="title-block text-left">
                                <ul class="socials mt-4">
                                    <li>
                                        <p class="pfooter font">Cursos Populares</p>
                                    </li>
                                    <li>
                                        <p class="pfooter font">Blog</p>
                                    </li>
                                </ul>
                            </div>
                         </div>
                    </div>
                    <div class="row">
                    <div class="col-12 mt-2 pl-0">
                            <!-- SUB Footer -->
                            <div class="sub-footer-copyright copyright-footer mb-4 pl-0 pr-3">
                                <span class="mb-3 mt-3" style="display: flex">
                                    <img src="{{ asset('images/logo_footer.png') }}" class="pl-0 pr-3 footerImg" style="margin: 20px 0 20px 0" />
                                    <p class="pfooter pl-2 pt-4 font">2019. Todos os Direitos Reservados. </p>
                                </span>
                            </div>
                            <!-- ... end SUB Footer -->
                        </div>
                    </div>
                </div>
            </div>
<!-- ... end Footer -->
</footer>
