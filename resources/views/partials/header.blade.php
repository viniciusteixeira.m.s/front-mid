<header class="container-fluid headerposition">
    <div class="row">
        <div class="col-3 text-center d-none d-xl-block">
            <img src="{{ asset('images/logo_topo.png') }}" class="mt-5 logo5120Header" style="width: 100px" />
        </div>
        <div class="col-6 col-lg-6 col-sm-6  float-left d-xl-none">
            <img src="{{ asset('images/logo_topo.png') }}" class="mt-5 logoWidth"/>
            {{-- <div class="col-9 d-block d-md-none">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="text-right">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </nav>
            </div> --}}
        </div>
        <div class="col-6 col-lg-6 col-sm-6 text-right d-xl-none">
            <img src="{{ asset('images/icone_menu.png') }}" class="mt-5 menuWidth"/>
        </div>
        <div class="col-5 d-none d-xl-block">
            <form class="inputheader">
                <input class="form-control mr-sm-2 mt-5 imglupa" type="search" placeholder="O QUE VOCÊ PROCURA?" aria-label="Search">
                <img src="{{ asset('images/lupa.svg')}}" class="ajustelupa">
            </form>
        </div> 
        <div class="col-12 d-xl-none mt-2">
            <form class="inputheader">
                <input class="form-control mr-sm-2 mt-3 imglupa" type="search" placeholder="O QUE VOCÊ PROCURA?" aria-label="Search">
                <img src="{{ asset('images/lupa.svg')}}" class="ajustelupa">
            </form>
        </div>   
        <div class="col-2 text-center mt-5 d-none d-xl-block btnCursos5120">
            <span class="input-group-btn">
                <img src="{{ asset('images/menu.svg')}}" class="cursosimg font" width="15" class="mr-2"/>
                <button class="btn btn-outline-success bgbtnlogin cursosbtn font header1200" type="submit" style="border-none">Cursos</button>
            </span>
        </div>
        <div class="col-2 text-left d-none d-xl-block botoesHeader5120">
            <button class="btn btn-outline-success mt-5 bgbtnlogin font header1200" style="color: #ebebeb" type="submit">Log in</button>
            <button class="btn btn-outline-success mt-5 font header1200" type="submit">Cadastre-se</button>
        </div>
    </div>

</header> 