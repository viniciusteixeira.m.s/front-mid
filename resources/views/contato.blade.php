@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid pr-0 pl-0">
    <img src="{{ asset('images/banner-contato.jpg') }}" class="d-none d-md-block bgbanner1"/>
    <img src="{{ asset('images/contato.jpg') }}" class="d-block d-md-none bgbanner1"/>
</div>

<div class="container-fluid">
    <div class="row mbcontato">
        <div class="col-xl-6 col-lg-12 offset-lg-1 d-none d-xl-block">
                    <form method="POST">
                        <h1 class="text-left text-white font-weight-bold font text-uppercase contatosobre h1Contato5120">Mande uma mensagem</h1>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="name" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Nome</label>
                            <div class="col-md-8 col-lg-10 font">
                                <input id="name" type="name" class="font form-control formContato @error('E-mail') is-invalid @enderror" name="name"  autofocus style="border-radius: 0">
                               
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="email" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Email</label>
                            <div class="col-md-8 col-lg-10">
                                <input id="email" type="email" class="font form-control formContato" name="email" style="border-radius: 0">
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group row" style="display: flow-root ">
                            <label for="textarea" class="col-md-4 col-form-label text-md-left text-white font labelContato5120">Mensagem</label>
                            <div class="col-md-8 col-lg-10">
                                <textarea id="textarea" type="textarea" class="font form-control formContato form textareacontato" name="textarea" style="border-radius: 0"></textarea>
                                    <span class="invalid-feedback" role="alert">
                                        <strong></strong>
                                    </span>
                            </div>
                        </div>

                            <div class="col-md-4 col-lg-10 text-right mt-5">
                                <button type="submit font" class="btn btn-outline-warning btnenviar">ENVIAR
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 d-block d-xl-none">
                        <form method="POST">
                            <h1 class="text-left text-white font-weight-bold font text-uppercase contatosobre mb-4">Mande uma mensagem</h1>
                            <div class="form-group row" style="display: flow-root ">
                                <div class="col-md-12 col-lg-12 font">
                                    <input id="name" type="name" class="font form-control formContato @error('E-mail') is-invalid @enderror" name="name" placeholder="Nome"  autofocus style="border-radius: 0">
                                   
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>
                            <div class="form-group row" style="display: flow-root ">
                                <div class="col-md-12 col-lg-12 font">
                                    <input id="email" type="email" class="font form-control formContato" name="email" placeholder="Email" style="border-radius: 0">
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>
                            <div class="form-group row" style="display: flow-root ">
                                <div class="col-md-12 col-lg-12 font">
                                    <textarea id="textarea" type="textarea" class="font form-control formContato form textareacontato" placeholder="Mensagem" name="textarea" style="border-radius: 0"></textarea>
                                        <span class="invalid-feedback" role="alert">
                                            <strong></strong>
                                        </span>
                                </div>
                            </div>
    
                                <div class="col-md-12 col-lg-12 text-right mt-5 mb-5 pb-5">
                                    <button type="submit font" class="btn btn-outline-warning btnenviar">ENVIAR
                                    </button>
                                </div>
                            </form>
                        </div>


                <div class="col-xl-4 col-lg-12 d-none d-xl-block">
                    <h1 class="text-left text-white font-weight-bold font text-left contatosobre mtmobile h1Contato5120"> ENTRE EM CONTATO</h1>
                    <h5 class="text-white mt-4 font-weight-bold h5Contato5120">SÃO PAULO</h5>
                        <p class="textoPolitica">Rua tal e tal, número 888, sala 888 <br/>
                        Perdizes. São Paulo, SP </p>
                        <p class="textoPolitica">email@estilomid.com.br</p>
                </div>
                <div class="col-xl-4 col-lg-12 d-block d-xl-none">
                    <h1 class="text-left text-white font-weight-bold font text-left contatosobre mtmobile mt-5 pt-5"> ENTRE EM CONTATO</h1>
                    <h5 class="text-white mt-4 font-weight-bold">SÃO PAULO</h5>
                        <p class="textoPolitica">Rua tal e tal, número 888, sala 888 <br/>
                        Perdizes. São Paulo, SP </p>
                        <p class="textoPolitica">email@estilomid.com.br</p>
                </div>
        </div>
        
    </div>
</div>

@endsection

@section('footer')
@endsection

@section('script')
@endsection