@extends('layouts.site')

@section('header')
@endsection

@section('content') 
<div class="container-fluid mt-5 pt-5">
 
    <div class="row text-white pt-3 offset-lg-1 ajusttitle">
        <div class="col-md-6 col-lg-4 d-none d-lg-block mbdiv">
            <ul class="timeline circle">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>
                </li>
            </ul>
            <ul class="timeline circle">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline">
                <li>
                    <a class="font font5120">INTRODUÇÃO AO CURSO</a>
                    <p class="colorp cortextos">Introdução ao curso - Leitura de 3 minutos.</p>                
                </li>
            </ul>
            <ul class="timeline lapis">
                <li class="bordersection">
                    <a class="font font5120">PRIMEIRA AVALIAÇÃO</a>
                    <p class="colorp">Duração aproximada: 30 minutos</p>                
                </li>
            </ul>
        </div>
            <div class="col-10 col-lg-7 col-sm-10 pl-sm-4 ajustmobAvaliacao font mbdiv">
                <p class="text-left titulocursos font d-md-none d-lg-block cortextos"> Tempo restante: <span class="bordersection font-weight-bolder"> 00:22:52</span></p>
                <div class="container-fluid d-md-block d-lg-none divAvaliacaoMobile">
                    <p class="text-left font pt-3 pl-4 pAvaliacaoMobile"> Tempo restante:  <span class="bordersection font-weight-bolder"> 00:22:52</span></p>
                </div>
                    <center class="row col-lg-12">
                            <div class="scrollbar text-left col-lg-12 col-sm-12">
                                
                                <form action="#">
                                    <p class="text-white text-uppercase pAvaliacao font-weight-bolder font5120">Pergunta da Avaliação</p>
                                    <p>
                                      <input type="radio" id="test1" name="radio-group" checked>
                                      <label for="test1" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                      <input type="radio" id="test2" name="radio-group">
                                      <label for="test2" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                      <input type="radio" id="test3" name="radio-group">
                                      <label for="test3" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test4" name="radio-group">
                                        <label for="test4" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                      </p>
                                      <p>
                                        <input type="radio" id="test5" name="radio-group">
                                        <label for="test5" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                      </p>
                                </form>
                                      <br/>
                                <form action="#">
                                      <p class="text-white text-uppercase pAvaliacao font-weight-bolder font5120">Pergunta da Avaliação</p>
                                    <p>
                                      <input type="radio" id="test6" name="radio-group" checked>
                                      <label for="test6" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                      <input type="radio" id="test7" name="radio-group">
                                      <label for="test7" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                      <input type="radio" id="test8" name="radio-group">
                                      <label for="test8" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test9" name="radio-group">
                                        <label for="test9" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                      </p>
                                      <p>
                                        <input type="radio" id="test10" name="radio-group">
                                        <label for="test10" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                      </p>
                                </form>
                                      <br/>
                                <form action="#">
                                      <p class="text-white text-uppercase pAvaliacao font-weight-bolder font5120">Pergunta da Avaliação</p>
                                    <p>
                                      <input type="radio" id="test11" name="radio-group" checked>
                                      <label for="test11" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                      <input type="radio" id="test12" name="radio-group">
                                      <label for="test12" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                      <input type="radio" id="test13" name="radio-group">
                                      <label for="test13" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test14" name="radio-group">
                                        <label for="test14" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                      </p>
                                      <p>
                                        <input type="radio" id="test15" name="radio-group">
                                        <label for="test15" class="labelAvaliacao cortextos"><span class="pAvaliacao5120">Alternativa</span></label>
                                      </p>
                                  </form>

                                <div class="overflow">
                                </div>
                            </div>
                    </center>
            </div>
@endsection

@section('footer')
@endsection

@section('script')
@endsection

