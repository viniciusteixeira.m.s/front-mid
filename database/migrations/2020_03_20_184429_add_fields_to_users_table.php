<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('document')->after('email')->nullable()->default(NULL);
            $table->string('profile')->after('document')->nullable()->default(NULL);
            $table->string('nickname')->after('profile')->nullable()->default(NULL);
            $table->string('group')->after('nickname')->nullable()->default(NULL);
            $table->string('department')->after('group')->nullable()->default(NULL);
            $table->string('occupation')->after('department')->nullable()->default(NULL);
            $table->string('birthday')->after('occupation')->nullable()->default(NULL);
            $table->string('gender')->after('birthday')->nullable()->default(NULL);
            $table->string('cep')->after('gender')->nullable()->default(NULL);
            $table->string('district')->after('cep')->nullable()->default(NULL);
            $table->string('city')->after('district')->nullable()->default(NULL);
            $table->string('place')->after('city')->nullable()->default(NULL);
            $table->string('number')->after('place')->nullable()->default(NULL);
            $table->string('state')->after('number')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
