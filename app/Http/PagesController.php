<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\BellNotifications;

class PagesController extends Controller
{

    public function index() 
    {
        return view('index');
    }

    public function sobre()
    {
        return view('sobre');
    }

    public function faq()
    {
        return view('faq');
    }

    public function termos()
    {
        return view('termos');
    }

    public function politica()
    {
        return view('politica');
    }

    public function pagamentoAprovado()
    {
        return view('pagamento-aprovado');
    }

    public function cursosSingle()
    {
        return view('cursos-single');
    }

    public function cursosMid()
    {
        return view('cursos-mid');
    }

    public function cursosLogin()
    {
        return view('cursos-login');
    }

    public function cursosConcluido()
    {
        return view('cursos-concluido');
    }

    public function cursosCadastro()
    {
        return view('cursos-cadastro');
    }

    public function cursosCadastroOk()
    {
        return view('cursos-cadastroOk');
    }

    public function cursosAvaliacao()
    {
        return view('cursos-avaliacao');
    }

    public function cursosAula()
    {
        return view('cursos-aula');
    }

    public function contato()
    {
        return view('contato');
    }
    public function loginAdm()
    {
        return view('login-adm');
    }
}